# Workshop Biometadata-01

Companion repository for the [Workshop: How to describe biological data? A primer to a FAIR approach for now and the future](https://biometadata-01.sciencesconf.org/).

The slides for the workshop are rendered as a website via GitLab Pages at: <https://clavellab.pages.rwth-aachen.de/workshop-biometadata-01>

## License

All materials on this repository are distributed under the [CC-BY-4.0 license](LICENSE).

## Funding

This workshop was made possible thanks to the support of the [NFDI4Microbiota](https://nfdi4microbiota.de), a consortium funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) — 460129525, that is part of the German NFDI (National research Data Infrastructure). One of the missions of the NFDI4Microbiota is to support the microbiology research community in making its data more FAIR via adequate tools and training.
